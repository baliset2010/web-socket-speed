package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/websocket"
)

type config struct {
	nrofMeters, updateCycle, meterStep int
	meterRandom                        bool
}

func sendMeters(ws *websocket.Conn, meters []int) {
	b := bytes.NewBuffer(make([]byte, 0, 4096))
	b.WriteString("METER:")
	for i := 0; i < len(meters)-1; i++ {
		b.WriteString(strconv.Itoa(meters[i]))
		b.WriteString(",")
	}
	b.WriteString(strconv.Itoa(meters[len(meters)-1]))
	ws.Write(b.Bytes())
}

func updateMeters(c *config, meters []int) {
	if c.meterRandom {
		for i := 0; i < len(meters); i++ {
			if rand.Intn(10) > 4 {
				meters[i]++
				if meters[i] > 100 {
					meters[i] = 100
				}
			} else {
				meters[i]--
				if meters[i] < 0 {
					meters[i] = 0
				}
			}
		}
	} else {
		for i := 0; i < len(meters); i++ {
			if meters[i] > 100 {
				meters[i] = 0
			} else {
				meters[i]++
			}
		}
	}
}

func meterProcess(c *config, ch <-chan int, ws *websocket.Conn) {
	meters := make([]int, c.nrofMeters)
	for i := 0; i < c.nrofMeters; i++ {
		if c.meterRandom {
			meters[i] = 50
		} else {
			meters[i] = 0
		}
	}

	sendMeters(ws, meters)
	for {
		select {
		case <-ch:
			return

		case <-time.After(time.Millisecond * time.Duration(c.updateCycle)):
			updateMeters(c, meters)
			sendMeters(ws, meters)
		}
	}
}

func meterServer(ws *websocket.Conn) {
	fmt.Println("meter server")
	c := config{240, 20, 1, false}

	var exit = false
	var ch chan int

	for exit == false {
		var msg string
		var err error
		if err := websocket.Message.Receive(ws, &msg); err != nil {
			fmt.Println("read error {}", err)
			return
		}
		if strings.HasPrefix(msg, "START:") {
			if ch == nil {
				fmt.Println("Start goroutine")
				ch = make(chan int)
				go meterProcess(&c, ch, ws)
			}

		} else if strings.HasPrefix(msg, "STOP:") {
			if ch != nil {
				fmt.Println("Stop goroutine")
				ch <- 1
				close(ch)
				ch = nil
			}

		} else if strings.HasPrefix(msg, "LEVEL:") {
			fmt.Println("change ", msg)
			ws.Write([]byte(msg))

		} else if strings.HasPrefix(msg, "METERS:") {
			if c.nrofMeters, err = strconv.Atoi(string(msg[7:])); err != nil {
				fmt.Println("meters number error {}", msg)
				return
			}
			fmt.Println("change ", msg)

		} else if strings.HasPrefix(msg, "CYCLE:") {
			if c.updateCycle, err = strconv.Atoi(string(msg[6:])); err != nil {
				fmt.Println("cycle number error {}", msg)
				return
			}
			fmt.Println("change ", msg)

		} else if strings.HasPrefix(msg, "RANDOM:") {
			c.meterRandom = (msg[7] == '1')
			fmt.Println("change ", msg)

		} else {
			fmt.Println("unknown command", msg)
			//return
		}
	}
}

func main() {
	// the following code will redirect itself forever, so it won't work
	//http.Handle("/meter", http.FileServer(http.Dir("index.html")))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	// the following code denies, because of its origin
	//http.Handle("/ws", websocket.Handler(meterServer))
	http.HandleFunc("/ws",
		func(w http.ResponseWriter, req *http.Request) {
			s := websocket.Server{Handler: websocket.Handler(meterServer)}
			s.ServeHTTP(w, req)
		})

	err := http.ListenAndServe(":5001", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
