# 次期卓評価用 GO サーバ #

### ファイル ###

* README.md		このファイル
* server.go		Go言語のソース
* index.html 	HTMLファイル
* server.XXX	ARM用にビルドされた実行バイナリ(XXX は arm/arm64/armbe/arm64be のいずれか)

### 実行バイナリを作る（実行バイナリ同梱のためオプション） ###

1. GO開発環境を揃える
2. go get golang.rog/x/net/websocket で WebSocket ライブラリをダウンロード
3. GOOS=linux GOARCH=arm go build server.go で実行バイナリ server を作成

GOARCH は以下のいずれかのはず。評価マシンの構成による

* arm
* arm64
* armbe			別途Cコンパイラが必要になるためバイナリを作成していない(よくわかっていない)
* arm64be		別途Cコンパイラが必要になるためバイナリを作成していない(よくわかっていない)
* なし			そのPCで実行できる

### 実行方法 ###

1. 評価マシンのIPを調べる。
2. index.html を編集し、 ws://127.0.0.1:5001/ws のところの '127.0.0.1' を上記で調べた IP アドレスにする
3. 評価マシンに index.html と server をコピーする
4. 評価マシンにログイン後、以下のコマンドを実行
	a. chmod +x server.XXX
	b. ./server.XXX
5. PCからブラウザを開いて、http://評価マシンのIP:5001 をアドレス・バーに入れてエンター・キーを押す
6. デモが動くはず


