A speed test for web socket and web app
=======================================

git clone git@bitbucket.org:baliset2010/web-socket-speed.git

based on https://qiita.com/okumurakengo/items/a8ccea065f5659d1a1de

which is based on https://www.youtube.com/watch?v=X7j6raOVXuU&list=PLYxzS__5yYQnRizvwNYWwzFjd9J4ni_Ga

1. install node-js
2. npm i ws --save
3. node index.js
4. open index.html with your browser


GO Version
==========
1. to build, GOOS=linux GOARCH=arm go build server.go
2. copy index.html at root to the same directory where 'server' exists
3. change IP address of index.html
4. run ./server
5. open browser with http://MACHINE_IP:5001


Protocol
========
index.html sends,...

* LEVEL:XX
	* XX:level value. server send it back to the client
* METERS:XX
	* XX:number of meters
* CYCLE:XX
	* XX:millisecond of cycle update meters
* RANDOM:X
	* X:0 or 1, meter change randomlly or not
* START:
	* start sending meter data
* STOP:
	* stop sending meter data


