var server = require('ws').Server;
var s = new server({port:5001});

var numOfMeters = 240;
var updateCycle = 20;
var meterValue = 0;
var meterStep = 1;
var meterRandom = false;
var meters = null;

var intervalTimer = null;

function createSameMeterMessage() {
	var msg = 'METER:';
	var items = String(meterValue) + ',';
	for(var i=0; i<(numOfMeters - 1); i++) {
		msg += items;
	}
	msg += String(meterValue);

	meterValue += meterStep;
	if (meterValue > 100) {
		meterValue = 0;
	} else if (meterValue < 0) {
		meterValue = 100;
	}
	return msg;
}

function getRandomValue(index) {
	value = meters[index];
	if (Math.random() > 0.5) {
		value = value + 1;
	} else {
		value = value - 1;
	}
	if (value > 100) {
		value = 100;
	}
	if (value < 0) {
		value = 0;
	}
	meters[index] = value;
	return value;
}

function createRandomMeterMessage() {
	var msg = 'METER:';
	var value;
	for(var i=0; i<(numOfMeters - 1); i++) {
		msg += String(getRandomValue(i)) + ',';
	}
	msg += String(getRandomValue(numOfMeters -1));
	return msg;
}

s.on('connection', function(ws){

    ws.on('message', function(message){
		if (message == 'START:') {
			console.log('START:' + message);
			if (intervalTimer) {
				clearInterval(intervalTimer);
			}
			meterValue = 0;
			intervalTimer = setInterval(function() {
				s.clients.forEach(function(client) {
					var msg;
					if (meterRandom) {
						msg = createRandomMeterMessage();
					} else {
						msg = createSameMeterMessage();
					}
					client.send(msg);
				});
			}, updateCycle);

		} else if (message == 'STOP:') {
			console.log('STOP:' + message);
			clearInterval(intervalTimer);
			intervalTimer = null;

		} else if (message.startsWith('LEVEL:')) {
			//console.log('LEVEL:' + message);
			s.clients.forEach(function(client) {
				client.send(message);
			});

		} else if (message.startsWith('METERS:')) {
			console.log(message);
			numOfMeters = parseInt(message.slice(7));
			console.log('numOfMeters:' + String(numOfMeters));

		} else if (message.startsWith('CYCLE:')) {
			console.log(message);
			updateCycle = parseInt(message.slice(6));
			console.log('updateCycle:' + String(updateCycle));

		} else if (message.startsWith('RANDOM:')) {
			console.log(message);
			if (message == 'RANDOM:1') {
				meters = [];
				for(var i=0; i<numOfMeters; i++) {
					meters.push(0);
				}
				meterRandom = true;
			} else {
				meterRandom = false;
			}
		} else {
			console.error('unknown command:' + message);
		}
    });

    ws.on('close',function(){
        console.log('I lost a client');
    });

});

